function generate_three_random_letters(){
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	for (var i = 0; i < 3; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	return text;
}
function generate_two_random_letters(){
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	for (var i = 0; i < 2; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	return text;
}
function generate_five_random_numbers(){
	var text = "";
	var possible = "1234567890";

	for (var i = 0; i < 5; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	return text;
}
var randomcode = generate_three_random_letters()+generate_five_random_numbers()+generate_two_random_letters();

$(document).ready(function() {
	setTimeout(function() {
		$('.gform_wrapper .ginput_container_post_title input').val(randomcode);
	}, 50);
	
	$('.gform_footer .gform_button').on('click',function(event){
		event.preventDefault();
		var language = $('.language-field select').val();
		var firstname = $('.name_first input').val();
		var lastname = $('.name_last input').val();
		$('.gform_wrapper .custom-language input').val(language);
		$('.gform_wrapper .custom-first-name input').val(firstname);
		$('.gform_wrapper .custom-last-name input').val(lastname);
		$('form').submit();
	});
});