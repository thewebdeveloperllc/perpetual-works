<?php
/*-----------------------------------------------------------------------------------*/
/* Custom Functions by Imaginate Studio
/*-----------------------------------------------------------------------------------*/

// #################### Faster than @import ####################
add_action( 'wp_enqueue_scripts', 'my_child_theme_scripts' );
function my_child_theme_scripts() {
    wp_enqueue_style( 'parent-theme-css', get_template_directory_uri() . '/style.css' );
}


/* #################### Rename Admin Links #################### */
// add_filter('gettext', 'rename_admin_menu_items');
// add_filter('ngettext', 'rename_admin_menu_items');
// function rename_admin_menu_items( $menu ) {
//      $menu = str_ireplace( 'Dashboard', 'Home', $menu ); //Rename the Dashboard to Home
//      return $menu;
// }


/* #################### Remove/Hide Items #################### */
function remove_menus(){    
     // remove_menu_page( 'index.php' );                  //Dashboard  
     // remove_menu_page( 'edit.php' );                   //Posts  
     // remove_menu_page( 'upload.php' );                 //Media  
     // remove_menu_page( 'edit.php?post_type=page' );    //Pages  
     // remove_menu_page( 'edit-comments.php' );          //Comments  
     // remove_menu_page( 'themes.php' );                 //Appearance  
     // remove_menu_page( 'plugins.php' );                //Plugins  
     // remove_menu_page( 'users.php' );                  //Users  
     // remove_menu_page( 'tools.php' );                  //Tools  
     // remove_menu_page( 'options-general.php' );        //Settings  
     // remove_menu_page( 'link-manager.php' );           //Links
     //remove_submenu_page( 'themes.php', 'theme-editor.php' ); //Theme Editor
    remove_action('admin_menu', '_add_themes_utility_last', 101); //Theme Editor
}

add_action( '_admin_menu', 'remove_menus' );


/*#################### Add User Role Class to Admin Body Tag ####################*/
function wpa66834_role_admin_body_class( $classes ) {
    global $current_user;
    foreach( $current_user->roles as $role )
        $classes .= ' role-' . $role;
    return trim( $classes );
}
add_filter( 'admin_body_class', 'wpa66834_role_admin_body_class' );

/*#################### Add Browser Class to Body Frontend ####################*/
// add_filter('body_class','add_role_to_body');
// function add_role_to_body($classes) {
//     $current_user = new WP_User(get_current_user_id());
//     $user_role = array_shift($current_user->roles);
//     $classes[] = 'role-'. $user_role;
//     return $classes;
// }

/*#################### Add Browser Class to Body ####################*/
// function mv_browser_body_class($classes) {
//         global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
//         if($is_lynx) $classes[] = 'lynx';
//         elseif($is_gecko) $classes[] = 'gecko';
//         elseif($is_opera) $classes[] = 'opera';
//         elseif($is_NS4) $classes[] = 'ns4';
//         elseif($is_safari) $classes[] = 'safari';
//         elseif($is_chrome) $classes[] = 'chrome';
//         elseif($is_IE) {
//                 $classes[] = 'ie';
//                 if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
//                 $classes[] = 'ie'.$browser_version[1];
//         } else $classes[] = 'unknown';
//         if($is_iphone) $classes[] = 'iphone';
//         if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
//                  $classes[] = 'osx';
//            } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
//                  $classes[] = 'linux';
//            } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
//                  $classes[] = 'windows';
//            }
//         return $classes;
// }
// add_filter('body_class','mv_browser_body_class');

/* Enable the dropdown option for hiding labels */
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );


/*ADMIN CSS*/
add_filter( 'admin_body_class', 'admin_user_id' );
function admin_user_id( $classes ) {
    // add 'class-name' to the $classes array
    global $current_user;
    $user_ID = $current_user->ID;
    $classes = 'user-id-' . $user_ID;
    // return the $classes array
    return $classes;
}
add_action('admin_head', 'admin_styles');

function admin_styles() {
     echo '<style>
            #toplevel_page_flatsome-panel, #wp-admin-bar-flatsome_panel, #wp-admin-bar-flatsome-activate, .post-type-page .wp-list-table .title .row-actions span:nth-child(2), h2#uxbuilder-enable-disable, #setting-error-tgmpa, #menu-appearance li.hide-if-no-customize {display: none !important;}
            body.user-id-1 #toplevel_page_flatsome-panel, body.user-id-1 #wp-admin-bar-flatsome_panel, body.user-id-1 h2#uxbuilder-enable-disable, body.user-id-1 #setting-error-tgmpa, body.user-id-1 #menu-appearance li.hide-if-no-customize {display: block !important;}
            body.user-id-1.post-type-page .wp-list-table .title .row-actions span:nth-child(2) {display: inline-block !important;}     </style>';
}

add_filter( 'body_class', 'frontend_user_id' );
function frontend_user_id( $classes ) {
    // add 'class-name' to the $classes array
    global $current_user;
    $user_ID = $current_user->ID;
    $classes[] = 'user-id-' . $user_ID;
    // return the $classes array
    return $classes;
}

/*-----------------------------------------------------------------------------------*/
/* You can add custom functions below */
/*-----------------------------------------------------------------------------------*/

register_post_type('secure', array(
	'label' => 'Secure',
	'menu_icon' => 'dashicons-location-alt',
	'description' => '',
	'public' => true,
	'show_ui' => true,
	'show_in_menu' => false,
	'capability_type' => 'page',
	'hierarchical' => false,
	'rewrite' => array('slug' => ''),
	'query_var' => true,	
	'supports' => array('title','editor','thumbnail','author','custom-fields'),
	'labels' => array (
		'name' => 'Secure',
		'singular_name' => 'Secure',
		'menu_name' => 'Secure',
		'add_new' => 'Add Item',
		'add_new_item' => 'Add Item',
		'edit' => 'Edit',
		'edit_item' => 'Edit Item',
		'new_item' => 'New Item',
		'view' => 'View Item',
		'view_item' => 'View Item',
		'search_items' => 'Search Items',
		'not_found' => 'No Items Found',
		'not_found_in_trash' => 'No Items Found in Trash',
		'parent' => 'Parent Item',
	),
));

function webdevco_scripts() {
	wp_enqueue_script( 'perpetual-jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js' );
	wp_enqueue_script( 'code-gen', get_stylesheet_directory_uri() . '/code-generator.js' );
}
add_action('wp_enqueue_scripts', 'webdevco_scripts');

/*-----------------------------------------------------------------------------------*/
/* Don't add any code below here or the sky will fall down */
/*-----------------------------------------------------------------------------------*/
?>